// site
// formattedAddress
// "246 Caspian Drive, Sunnyvale, CA 94089"
// geometry
// lat
// 37.412211060172915
// lng
// -122.01487362384796
// siteBoundary

export const model1 = {
  "address": "246 Caspian Drive, Sunnyvale, CA 94089",
  "type":"Feature",
  "lat": 37.412211060172915,
  "lng": -122.01487362384796,
  "properties":null,
  "geometry":{
    "type":"Polygon",
    "coordinates":[
      [
        [ -122.0156649, 37.4134748 ], 
        [ -122.016116, 37.4121126 ], 
        [ -122.0162848, 37.4121481 ], 
        [ -122.0165031, 37.4114898 ], 
        [ -122.0141078, 37.4109691 ], 
        [ -122.0134327, 37.4130043 ], 
        [ -122.0156649, 37.4134748 ]
      ]
    ]
  }
}

export const model2 = {
  "address": "1176 University Ave Berkeley CA 94707 Block",
  "type": "Feature",
  "lat": 37.86934975199577,
  "lng": -122.28956912385254,
  "properties": {},
  "geometry": {
    "type": "Polygon",
    "coordinates": [
      [
        [
          -122.28956912385254,
          37.86934975199577
        ],
        [
          -122.29201279627563,
          37.86904846816438
        ],
        [
          -122.2917317066643,
          37.8681847366158
        ],
        [
          -122.28940149195682,
          37.86849679808114
        ],
        [
          -122.28956912385254,
          37.86934975199577
        ]
      ]
    ]
  }
}
