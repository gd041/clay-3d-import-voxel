import { getSites_sites as SiteType } from '../designs/lib/types'

import {
    Point,
    OffsetEdge,
    ProjectType
} from '../designs/lib/types'

/**
 * @ol_path web/packages/clay/src/apps/briefs/lib/test-data.ts
 * @description
 * - 450 W Santa Clara briefs site from Firestore
 * - template for 3D import
 */

export const site1: SiteType = {
    __typename: "Site",
    id: "M17GLQZYV8B2Pglj0qdArmRE5D4nj3wa",
    formattedAddress: '450 W Santa Clara St, San Jose, CA 95110',
    geometry: {
        __typename: "SiteGeometry",
        lat: 37.332296,
        lng: -121.897828,
        siteBoundary: `{
            "type": "Feature",
            "geometry": {
                "type": "Polygon",
                "coordinates": [
                    [
                        [
                            -121.89955817111,
                            37.33147675195
                        ],
                        [
                            -121.900086423324,
                            37.331453022288
                        ],
                        [
                            -121.900112707869,
                            37.331829269534
                        ],
                        [
                            -121.90011274584,
                            37.331832862055
                        ],
                        [
                            -121.900112412197,
                            37.331836437388
                        ],
                        [
                            -121.900111676003,
                            37.33183997067
                        ],
                        [
                            -121.900110565499,
                            37.331843463365
                        ],
                        [
                            -121.900109081643,
                            37.331846846072
                        ],
                        [
                            -121.900107222599,
                            37.331850141341
                        ],
                        [
                            -121.900105045333,
                            37.331853257475
                        ],
                        [
                            -121.900102492036,
                            37.331856241123
                        ],
                        [
                            -121.900099649415,
                            37.331859021859
                        ],
                        [
                            -121.90009651479,
                            37.331861577185
                        ],
                        [
                            -121.900093119048,
                            37.331863929263
                        ],
                        [
                            -121.900089432009,
                            37.331866033393
                        ],
                        [
                            -121.90008556876,
                            37.331867888202
                        ],
                        [
                            -121.900081498664,
                            37.331869424664
                        ],
                        [
                            -121.900077308333,
                            37.331870687705
                        ],
                        [
                            -121.900072969137,
                            37.331871655137
                        ],
                        [
                            -121.900068567956,
                            37.331872325923
                        ],
                        [
                            -121.900064074309,
                            37.331872699525
                        ],
                        [
                            -121.899570666715,
                            37.331894456587
                        ],
                        [
                            -121.899464779561,
                            37.331899154599
                        ],
                        [
                            -121.899487290977,
                            37.331732796972
                        ],
                        [
                            -121.899512655075,
                            37.331732678808
                        ],
                        [
                            -121.899529849298,
                            37.331629986722
                        ],
                        [
                            -121.899532276293,
                            37.331615498424
                        ],
                        [
                            -121.89955817111,
                            37.33147675195
                        ]
                    ]
                ]
            },
            "properties": null
        }`
    },
    displayName: "450 W Santa Clara St ",
    siteType: ProjectType.AdaptiveReuse,
    thumbnail: "thumbnail.png",
    zoning: {
        __typename: "Zoning",
        maxHeight: 10,
        // maxHeightOverride: {
        //   __typename: "OverridableFloat",
        // //   overridden: OverrideState.Default,
        //   value: 10,
        // },
        setbacks: {
          __typename: "Setbacks",
        //   areaSqFt: 20.0,
          simplifiedPoly: [
            {
              point: [-121.89955817111, 37.331476751949985],
              offset: 10,
              __typename: "OffsetEdge",
            },
            {
              point: [-121.900086423324, 37.331453022288],
              offset: 10,
              __typename: "OffsetEdge",
            },
            {
              point: [-121.900107222599, 37.33185014134101],
              offset: 10,
              __typename: "OffsetEdge",
            },
            {
              point: [-121.89946477956099, 37.33189915459899],
              offset: 10,
              __typename: "OffsetEdge",
            },
            {
              point: [-121.89953227629302, 37.331615498424],
              offset: 10,
              __typename: "OffsetEdge",
            },
          ],
        //   overridden: OverrideState.Overridden,
        },
        targetFAR: 4,
        // targetFAROverride: {
        //   __typename: "OverridableFloat",
        //   overridden: OverrideState.Default,
        //   value: 4,
        // },
        maximumLotCoverage: 0.8,
        // maximumLotCoverageOverride: {
        //   __typename: "OverridableFloat",
        //   overridden: OverrideState.Default,
        //   value: 0.8,
        // },
      },
    }

    export const site2: SiteType = {
        __typename: "Site",
        id: "2bwV3xGoY6eKgmOyQOXyAEzWM8ZBQd7k",
        formattedAddress: '384 W Santa Clara St, San Jose, CA 95113',
        geometry: {
            __typename: "SiteGeometry",
            lat: 37.331348,
            lng: -121.898027,
            siteBoundary: `{
                "type": "Feature",
                "geometry": {
                    "type": "Polygon",
                    "coordinates": [
                        [
                            [
                                -121.897166515201,
                                37.331487940308
                            ],
                            [
                                -121.897111191082,
                                37.331400496792
                            ],
                            [
                                -121.897010836963,
                                37.331239632669
                            ],
                            [
                                -121.896928313165,
                                37.331163112003
                            ],
                            [
                                -121.896894975951,
                                37.330850261949
                            ],
                            [
                                -121.896973495517,
                                37.330813846808
                            ],
                            [
                                -121.897013578769,
                                37.330792905158
                            ],
                            [
                                -121.897052782576,
                                37.330770921408
                            ],
                            [
                                -121.897091019638,
                                37.330747874072
                            ],
                            [
                                -121.897123820374,
                                37.330723791205
                            ],
                            [
                                -121.897238374813,
                                37.330646156507
                            ],
                            [
                                -121.897242790113,
                                37.33064358779
                            ],
                            [
                                -121.897247319571,
                                37.330641149283
                            ],
                            [
                                -121.897251956452,
                                37.330638842868
                            ],
                            [
                                -121.897256695198,
                                37.330636673118
                            ],
                            [
                                -121.897261529074,
                                37.330634641915
                            ],
                            [
                                -121.897266452506,
                                37.330632752931
                            ],
                            [
                                -121.897271458741,
                                37.330631007146
                            ],
                            [
                                -121.897276542204,
                                37.330629408233
                            ],
                            [
                                -121.897281693904,
                                37.3306279581
                            ],
                            [
                                -121.897286908233,
                                37.330626658618
                            ],
                            [
                                -121.897292178438,
                                37.330625510767
                            ],
                            [
                                -121.8972974978,
                                37.330624517331
                            ],
                            [
                                -121.89730285842,
                                37.330623678405
                            ],
                            [
                                -121.897308253564,
                                37.33062299587
                            ],
                            [
                                -121.897313676461,
                                37.330622469809
                            ],
                            [
                                -121.897319119248,
                                37.330622102116
                            ],
                            [
                                -121.897324576265,
                                37.330621891959
                            ],
                            [
                                -121.897330038521,
                                37.330621841246
                            ],
                            [
                                -121.897335499213,
                                37.330621948257
                            ],
                            [
                                -121.897340951586,
                                37.330622213974
                            ],
                            [
                                -121.897351801995,
                                37.330623219169
                            ],
                            [
                                -121.8973692256,
                                37.33062504355
                            ],
                            [
                                -121.897386717875,
                                37.330626375971
                            ],
                            [
                                -121.897404258476,
                                37.330627214872
                            ],
                            [
                                -121.897421824821,
                                37.330627559621
                            ],
                            [
                                -121.897439395456,
                                37.330627409573
                            ],
                            [
                                -121.897456948959,
                                37.330626765884
                            ],
                            [
                                -121.897474462748,
                                37.330625627922
                            ],
                            [
                                -121.897491915419,
                                37.330623997745
                            ],
                            [
                                -121.897509285551,
                                37.33062187651
                            ],
                            [
                                -121.897526551775,
                                37.330619268077
                            ],
                            [
                                -121.897560683579,
                                37.330612602388
                            ],
                            [
                                -121.897856317225,
                                37.33054901319
                            ],
                            [
                                -121.8989342734,
                                37.331986404032
                            ],
                            [
                                -121.898937132076,
                                37.331990627045
                            ],
                            [
                                -121.898939507898,
                                37.331995039661
                            ],
                            [
                                -121.898941399987,
                                37.331999595031
                            ],
                            [
                                -121.898942805667,
                                37.332004270655
                            ],
                            [
                                -121.898943668789,
                                37.332009021246
                            ],
                            [
                                -121.89894401645,
                                37.332013847379
                            ],
                            [
                                -121.898943848309,
                                37.332018610281
                            ],
                            [
                                -121.898943163407,
                                37.33202337935
                            ],
                            [
                                -121.898941933384,
                                37.332028086438
                            ],
                            [
                                -121.898940212666,
                                37.332032684935
                            ],
                            [
                                -121.898937974881,
                                37.332037152626
                            ],
                            [
                                -121.898935274437,
                                37.332041442001
                            ],
                            [
                                -121.898932112058,
                                37.332045531423
                            ],
                            [
                                -121.898928515074,
                                37.332049373704
                            ],
                            [
                                -121.898924539496,
                                37.332052946548
                            ],
                            [
                                -121.898920158227,
                                37.332056249377
                            ],
                            [
                                -121.898915427512,
                                37.33205921213
                            ],
                            [
                                -121.89891040311,
                                37.332061859373
                            ],
                            [
                                -121.898273299324,
                                37.33236419216
                            ],
                            [
                                -121.898194253921,
                                37.33240169043
                            ],
                            [
                                -121.897462133828,
                                37.331345198769
                            ],
                            [
                                -121.897166515201,
                                37.331487940308
                            ]
                        ]
                    ]
                },
                "properties": null
            }`
        },
        displayName: "384 W Santa Clara St",
        siteType: ProjectType.AdaptiveReuse,
        thumbnail: "thumbnail.png",
        zoning: {
            __typename: "Zoning",
            maxHeight: 10,
            // maxHeightOverride: {
            //   __typename: "OverridableFloat",
            // //   overridden: OverrideState.Default,
            //   value: 10,
            // },
            setbacks: {
              __typename: "Setbacks",
            //   areaSqFt: 20.0,
              simplifiedPoly: [
                {
                    point: [-121.89879181490535, 37.33197922140366],
                    offset: 10,
                    __typename: "OffsetEdge",
                  },
                  {
                    point: [-121.89811685813496, 37.33102298458151],
                    offset: 10,
                    __typename: "OffsetEdge",
                  },
                  {
                    point: [-121.89755975095969, 37.33128067989004],
                    offset: 10,
                    __typename: "OffsetEdge",
                  },
                  {
                    point: [-121.89822131573041, 37.33224969152607],
                    offset: 10,
                    __typename: "OffsetEdge",
                  },
                  {
                    point: [-121.89879181490535, 37.33197922140366],
                    offset: 10,
                    __typename: "OffsetEdge",
                  },
              ],
            //   overridden: OverrideState.Overridden,
            },
            targetFAR: 4,
            // targetFAROverride: {
            //   __typename: "OverridableFloat",
            //   overridden: OverrideState.Default,
            //   value: 4,
            // },
            maximumLotCoverage: 0.8,
            // maximumLotCoverageOverride: {
            //   __typename: "OverridableFloat",
            //   overridden: OverrideState.Default,
            //   value: 0.8,
            // },
          },
        }














//   export const brief: FulfilledGetBrief = {
//     brief: {
//       __typename: "Brief",
//       id: "1",
//       application: "",
//       meta: {
//         name: "My Brief",
//         createdAt: "123",
//         updatedAt: "123",
//         thumbnail: "123",
//         __typename: "Meta",
//       },
//       construction: {
//         __typename: "Construction",
//         start: null,
//         end: null,
//       },
//       openSpace: {
//         __typename: "OpenSpace",
//         minOpenSpace: 20,
//         targetOpenSpace: 25,
//         minOpenSpaceOverride: {
//           __typename: "OverridableFloat",
//           overridden: OverrideState.Default,
//           value: 0.1,
//         },
//         targetOpenSpaceOverride: {
//           __typename: "OverridableFloat",
//           overridden: OverrideState.Default,
//           value: 0.1,
//         },
//       },
//       briefSite: {
//         __typename: "BriefSite",
//         maximumAllowableBuildingArea: 230000,
//         developmentPadArea: 10000,
//         developmentEnvelopeAreaSqFt: 12000,
//         allowances: {
//           __typename: "Allowances",
//           exteriorLossFactor: 0.1,
//           exteriorLossFactorOverride: {
//             __typename: "OverridableFloat",
//             overridden: OverrideState.Default,
//             value: 0.1,
//           },
//           interiorLossFactor: 0.15,
//           interiorLossFactorOverride: {
//             __typename: "OverridableFloat",
//             overridden: OverrideState.Default,
//             value: 0.15,
//           },
//         },
//         site: {
//           __typename: "Site",
//           thumbnail: "thumbnail.png",
//           id: "M17GLQZYV8B2Pglj0qdArmRE5D4nj3wa",
//           totalLotArea: 230000,
//           displayName: "123 Main St.",
//           formattedAddress: "123 Main St.",
//           geometry: {
//             __typename: "SiteGeometry",
//             lat: 37.332296,
//             lng: -121.897828,
//             siteBoundary: `{
//             "type": "Feature",
//             "geometry": {
//               "type": "Polygon",
//               "coordinates": [
//                 [
//                   [-121.89955817111, 37.33147675195],
//                   [-121.900086423324, 37.331453022288],
//                   [-121.900112707869, 37.331829269534],
//                   [-121.90011274584, 37.331832862055],
//                   [-121.900112412197, 37.331836437388],
//                   [-121.900111676003, 37.33183997067],
//                   [-121.900110565499, 37.331843463365],
//                   [-121.900109081643, 37.331846846072],
//                   [-121.900107222599, 37.331850141341],
//                   [-121.900105045333, 37.331853257475],
//                   [-121.900102492036, 37.331856241123],
//                   [-121.900099649415, 37.331859021859],
//                   [-121.90009651479, 37.331861577185],
//                   [-121.900093119048, 37.331863929263],
//                   [-121.900089432009, 37.331866033393],
//                   [-121.90008556876, 37.331867888202],
//                   [-121.900081498664, 37.331869424664],
//                   [-121.900077308333, 37.331870687705],
//                   [-121.900072969137, 37.331871655137],
//                   [-121.900068567956, 37.331872325923],
//                   [-121.900064074309, 37.331872699525],
//                   [-121.899570666715, 37.331894456587],
//                   [-121.899464779561, 37.331899154599],
//                   [-121.899487290977, 37.331732796972],
//                   [-121.899512655075, 37.331732678808],
//                   [-121.899529849298, 37.331629986722],
//                   [-121.899532276293, 37.331615498424],
//                   [-121.89955817111, 37.33147675195]
//                 ]
//               ]
//             },
//             "properties": {
//               "situs_street_type": "ST",
//               "situs_state_code": "CA",
//               "shape_area": "26059.140144933619",
//               "objectid": "449547",
//               "number_of_situs_address": "1",
//               "situs_street_direction": "W",
//               "situs_city_name": "SAN JOSE",
//               "shape_length": "651.51246171666992",
//               "ap_lp": "LP",
//               "situs_house_number": "450",
//               "tax_rate_area": "17213",
//               "apn": "25938132",
//               "situs_house_number_suffix": null,
//               "situs_street_name": "SANTA CLARA",
//               "situs_unit_number": null,
//               "situs_zip_code": "95112"
//             }
//           }`,
//           },
//         },
//         zoning: {
//           __typename: "Zoning",
//           maxHeight: 10,
//           maxHeightOverride: {
//             __typename: "OverridableFloat",
//             overridden: OverrideState.Default,
//             value: 10,
//           },
//           setbacks: {
//             __typename: "Setbacks",
//             areaSqFt: 20.0,
//             simplifiedPoly: [
//               {
//                 point: [-121.89955817111, 37.331476751949985],
//                 offset: 10,
//                 __typename: "OffsetEdge",
//               },
//               {
//                 point: [-121.900086423324, 37.331453022288],
//                 offset: 10,
//                 __typename: "OffsetEdge",
//               },
//               {
//                 point: [-121.900107222599, 37.33185014134101],
//                 offset: 10,
//                 __typename: "OffsetEdge",
//               },
//               {
//                 point: [-121.89946477956099, 37.33189915459899],
//                 offset: 10,
//                 __typename: "OffsetEdge",
//               },
//               {
//                 point: [-121.89953227629302, 37.331615498424],
//                 offset: 10,
//                 __typename: "OffsetEdge",
//               },
//             ],
//             overridden: OverrideState.Overridden,
//           },
//           targetFAR: 4,
//           targetFAROverride: {
//             __typename: "OverridableFloat",
//             overridden: OverrideState.Default,
//             value: 4,
//           },
//           maximumLotCoverage: 0.8,
//           maximumLotCoverageOverride: {
//             __typename: "OverridableFloat",
//             overridden: OverrideState.Default,
//             value: 0.8,
//           },
//         },
//       }
//     },
//   };

