import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { AxesHelper, Color, GridHelper, PerspectiveCamera, PointLight, Raycaster, Scene, WebGLRenderer } from "three";
import { TransformControls } from 'three/examples/jsm/controls/TransformControls';

export const makeScene = (): 
  {
    scene: Scene,
    renderer: WebGLRenderer,
    raycaster: Raycaster,
    camera: PerspectiveCamera,
    controls: OrbitControls,
    transformControls: TransformControls,
  } => {
  
  const scene = new Scene();

  scene.add(new AxesHelper(600))

  //adjust background color
  scene.background = new Color("white");

  //lighting
  const light = new PointLight()
  light.position.set(2.5, 7.5, 15)
  scene.add(light)

  //grid 
  const size: number = 1000;
  const divisions: number = 20;
  const gridhelper = new GridHelper(size, divisions, "black");
  scene.add(gridhelper)

  //camera config.
  const camera = new PerspectiveCamera(
    50,
    window.innerWidth / window.innerHeight,
    0.1,
    2000
  )
  camera.position.z = 300 //300
  camera.position.y = 700 //500
  camera.position.x = 300 //300

  const renderer = new WebGLRenderer({antialias: true});
  //renderer
  renderer.setSize(window.innerWidth, window.innerHeight)
  document.body.appendChild(renderer.domElement)

  const controls = new OrbitControls(camera, renderer.domElement)
  controls.enableDamping = true;

  // transform controls added to imported object
  const transformControls = new TransformControls(camera, renderer.domElement)
  scene.add(transformControls);

  window.addEventListener('keypress', (event: KeyboardEvent) => {
    switch (event.code) {
        case 'KeyT':
            transformControls.setMode('translate')
            break
        case 'KeyR':
            transformControls.setMode('rotate')
            break
        case 'KeyS':
            transformControls.setMode('scale')
            break
    }
  })

  return {
    scene,
    raycaster: new Raycaster(),
    renderer,
    camera,
    controls,
    transformControls,
  };
}