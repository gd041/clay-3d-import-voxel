import { Euler, Mesh, Object3D, Vector3 } from "three";
// imports related to site generation
import { siteMesh, devPadMesh, devPad } from '../../designs/lib/scene';
import { feetToMeter } from "../utils/points";

export const getSite = () => {
  const siteWork = new Object3D()

  devPadMesh!.position.y += 1;
  // add site & devPad to 3D editor
  siteWork.add(siteMesh!.rotateOnAxis(new Vector3(1,0,0), -90*Math.PI/180))
  siteWork.add(devPadMesh!.rotateOnAxis(new Vector3(1,0,0), -90*Math.PI/180));
  return siteWork;
}

// return true if a vector is inside the polygon (here the polygon is the site)
const inside = (point: Vector3) => {
  const vs = devPad.vertices;
  const {x, z: y} = point;
  
  let inside = false;
  for (let i = 0, j = vs.length - 1; i < vs.length; j = i++) {
      var xi = vs[i].x, yi = -vs[i].y;
      var xj = vs[j].x, yj = -vs[j].y;
      
      var intersect = ((yi > y) != (yj > y))
          && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
      if (intersect) inside = !inside;
  }
  
  return inside;
};

const vecs = [
  new Vector3(feetToMeter(15), 0, feetToMeter(15)),
  new Vector3(feetToMeter(15), 0, -feetToMeter(15)),
  new Vector3(-feetToMeter(15), 0, -feetToMeter(15)),
  new Vector3(-feetToMeter(15), 0, feetToMeter(15))
]

// return true if all vertices of a voxel are inside the polygon
const verticesContained = (center: Vector3, angle: number, grid = 30): boolean => {
  const euler = new Euler(0, angle, 0);
  const p0 = new Vector3().copy(vecs[0]).applyEuler(euler).add(center);
  const p1 = new Vector3().copy(vecs[1]).applyEuler(euler).add(center);
  const p2 = new Vector3().copy(vecs[2]).applyEuler(euler).add(center);
  const p3 = new Vector3().copy(vecs[3]).applyEuler(euler).add(center);
  return inside(p0) && inside(p1) && inside(p2) && inside(p3);
};

// make the voxels out of the devpad invisible
export const chooseVoxelsOnDevPad = (voxel: Mesh, voxelCenter: Vector3, angle: number): void => {

    let isIn = verticesContained(voxelCenter, angle);
    if (voxel.visible && !isIn) voxel.visible = false;
}

// return true if the whole building is on the site
export const isBuildingOnSite = (building: Mesh): boolean => {
  const positionAttribute = building.geometry.getAttribute('position');
  const vertex = new Vector3();
  let isOnSite = true;
  for(let i = 0; i < positionAttribute.count; i++) {
    vertex.fromBufferAttribute(positionAttribute, i);
    vertex.applyMatrix4(building.matrix);
    // console.log(vertex);

    isOnSite = inside(vertex);
    if (!isOnSite) return false;
  }
  return true;
}
