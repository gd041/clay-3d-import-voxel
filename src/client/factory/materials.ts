import { DoubleSide, EdgesGeometry, LineBasicMaterial, LineSegments, Mesh, MeshBasicMaterial, PointsMaterial } from "three";

// set default material of imported model
export const defaultMaterial = new MeshBasicMaterial({ 
    color: 'gray', 
    transparent: true,
    polygonOffset: true,
    polygonOffsetFactor: 1, // positive value pushes polygon further away
    polygonOffsetUnits: 1,
    opacity: 0.4,
    side: DoubleSide,
})


export const getWireFrame = (mesh: Mesh, color: string): LineSegments => {
  //add wireframe linework
  const lineGeo = new EdgesGeometry(mesh.geometry)
  const lineMaterial = new LineBasicMaterial({color, polygonOffset: true, polygonOffsetFactor: 0.2, polygonOffsetUnits: 0.5})
  return new LineSegments(lineGeo, lineMaterial)
}

export const pointMaterial = new PointsMaterial({
  size: 10,
  color: 'red'
})