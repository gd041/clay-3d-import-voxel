import { Color } from "three";

export const settings = {
  voxelSize: 30,
  normalColor: new Color(0x888888),
  warningColor: new Color(0xff5555)
}