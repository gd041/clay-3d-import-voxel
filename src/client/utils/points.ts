import { BoxGeometry, BufferGeometry, DoubleSide, Mesh, MeshBasicMaterial, Object3D, Points, PointsMaterial, Raycaster, Vector3 } from "three"
import { getWireFrame, pointMaterial } from "../factory/materials"


export const feetToMeter = (feet: number) => feet / 3.28;
export const meterToFeet = (meter: number) => meter * 3.28; 

const voxelSize = new Vector3(feetToMeter(30), feetToMeter(15), feetToMeter(30));

//create point cloud
export const createPtCloud = (): {points: Vector3[], pointMeshes: Points, voxels: Object3D}  => {
  let points: Vector3[] = []
  let matl = new PointsMaterial({
      size: 2,
      vertexColors: true,
      color: 'red'
  })

  const voxels = new Object3D();
  const voxel = new Mesh(
    new BoxGeometry(feetToMeter(30), feetToMeter(15), feetToMeter(30)),
    new MeshBasicMaterial({
      color: 'blue',
      transparent: false,
      opacity: 1.0,
      side: DoubleSide
  }));
  const edge = getWireFrame(voxel, "white");
  voxel.add(edge);
  for(let x = -20; x < 20; x++) {
      for (let y = 0; y < 20; y++) {
          for(let z = -20; z < 20; z++ ) {
              let pt = new Vector3(x*feetToMeter(30), y*feetToMeter(15), z*feetToMeter(30)*-1)
              points.push( pt );
              const newVoxel = voxel.clone();
              newVoxel.position.copy(pt).add(new Vector3(feetToMeter(15), feetToMeter(7.5), feetToMeter(15)))
              newVoxel.visible = false;
              voxels.add(newVoxel);
          }
      }
  }
  let geo = new BufferGeometry().setFromPoints(points)
  let pts = new Points(geo, matl);

  return {points, pointMeshes: pts, voxels: voxels}
}

const direction = new Vector3(0, 1, 0);

/**
 * @source
 * @description
 * @param pts 
 * @param mesh 
 */
export const getPointsInMesh = (raycaster: Raycaster, pts: Vector3[], mesh: Mesh): {inPoints: Vector3[], inPointsMesh: Points} => {

  let temp: Vector3[] = []

  pts.forEach(pt => {
    const isIn = isInMesh(pt, mesh, raycaster);
    if(isIn) temp.push(pt);
  })

  let geo = new BufferGeometry().setFromPoints(temp)
  let ptsInMesh = new Points(geo, pointMaterial)  
  return { inPointsMesh: ptsInMesh, inPoints: temp};
}

export const isInMesh = (point: Vector3, mesh: Mesh, raycaster: Raycaster):boolean => {
  raycaster.set(point, direction)
  let intersects = raycaster.intersectObject(mesh, false)

  intersects = intersects.filter((item, pos, ary) => {
      return !pos || Math.abs(item.distance - ary[pos - 1].distance) > 0.1;
  })
  intersects = intersects.filter(item => item.distance>1)

  return intersects.length % 2 === 1;
}