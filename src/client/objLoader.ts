import { MeshBasicMaterial } from 'three';
import { BackSide, DoubleSide, FrontSide, Mesh, Vector2, Vector3 } from 'three'
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader'
import Stats from 'three/examples/jsm/libs/stats.module'
import { GUI } from 'dat.gui'
import { makeScene } from './factory/scene'
import { chooseVoxelsOnDevPad, getSite, isBuildingOnSite } from './factory/site'
import { defaultMaterial, getWireFrame } from './factory/materials'
import { createPtCloud, isInMesh } from './utils/points'
import { settings } from './settings';

/**
 * @source https://stackoverflow.com/questions/44630265/how-can-i-set-z-up-coordinate-system-in-three-js
 * @description possible approach to switch Y-up to Z-up coordinate system 
 * this has a series of implications such as 
 * - - will need to rotate grids, 
 * - - ensure that the .obj import does not map Rhino Z to OBJ Y 
 */

const {scene, renderer, raycaster, camera, controls, transformControls} = makeScene();

const siteWork = getSite();
scene.add(siteWork);

let gridRotation = 0;

const {points, pointMeshes, voxels} = createPtCloud();
pointMeshes.visible = false;
scene.add(voxels);
scene.add(pointMeshes);

let inMeshCount = 0;
const countDOM = document.getElementById("count");
countDOM!.innerText = "Voxel count: " + inMeshCount;

let object: Mesh | null = null;

const TEST_MODELS = {
    test3: 'models/221009_test3.obj',
    test4: 'models/221018_test4_model_and_site.obj',
    test5: 'models/221018_test5_model_and_site.obj',
    test6: 'models/221018_test6_model_and_site.obj',
    test7: 'models/221018_test7_model_and_site.obj'
}
 
// const file = TEST_MODELS.test3

function loadObj(file: string) { 
    new OBJLoader().load(
        file,
        (group) => {
            object = group.children[0] as Mesh

            object.geometry.center();
            object.geometry.translate(0, object.geometry.boundingBox?.max.y ?? 0, 0);
            
            object.material = defaultMaterial
            
            const wireframe = getWireFrame(object, "black");
            object.add(wireframe)
            
            scene.add(group)
            transformControls.attach(group);
            transformControls.visible = false;

            regenerate()
        },
        (xhr) => {
            console.log((xhr.loaded / xhr.total) * 100 + '% loaded')
        },
        (error) => {
            console.log(error)
        }
    )
}
            
loadObj(TEST_MODELS.test4);

transformControls.addEventListener('dragging-changed', (e) => {
    controls.enabled = !e.value;
    regenerate()
});

/**
 * 
 * @param e event 
 * @source https://threejs.org/docs/?q=raycaster#api/en/core/Raycaster
 * @description calculate pointer position in normalized device coords
 */
const mouse = new Vector2()
window.addEventListener('mousemove', (e) => {
    mouse.x = (e.clientX / renderer.domElement.clientWidth) * 2 - 1,
    mouse.y = -(e.clientY / renderer.domElement.clientHeight) * 2 + 1
}, false);

window.addEventListener('resize', onWindowResize, false)
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight
    camera.updateProjectionMatrix()
    renderer.setSize(window.innerWidth, window.innerHeight)
    render()
}

//planar faces
const options = {
    side: {
        "DoubleSide": DoubleSide,
        "FrontSide": FrontSide,
        "BackSide": BackSide,
    }
}

// controls
const gui = new GUI()
const materialFolder = gui.addFolder('Material')

const params = {
    grid: false,
    rotateBuilding: false,
    rotateGrid: false,
    voxels: true,
    transform: false,
}

materialFolder.add(defaultMaterial, 'opacity', 0, 1, 0.01)
// materialFolder.add(params, 'grid').onChange(() => {
//     params.grid != params.grid
//     pointMeshes.visible = params.grid
// })
materialFolder.add(params, 'rotateBuilding').onChange(() => {
    params.rotateBuilding != params.rotateBuilding;
    if (object) object.rotation.y %= (Math.PI * 2);
})
materialFolder.add(params, 'rotateGrid').onChange(() => {
    params.rotateGrid != params.rotateGrid;
})
materialFolder.add(params, 'voxels').onChange(() => {
    params.voxels != params.voxels;
    voxels.visible = params.voxels;
});
materialFolder.add(params, 'transform').onChange(() => {
    params.transform != params.transform
    transformControls.visible = params.transform
})
materialFolder.open()

// STATS
const stats = Stats()
document.body.appendChild(stats.dom)

function animate() {

    requestAnimationFrame(animate)
    controls.update()
    render()
    stats.update()
    if(params.rotateBuilding || params.rotateGrid)
        regenerate();

}

function regenerate() {
    if (!object) return;
    if (params.rotateBuilding){
        object.rotation.y += 0.01;
        object.updateMatrix();
        object.updateMatrixWorld();    
    } else if (params.rotateGrid) {
        gridRotation += 0.01;
        gridRotation %= Math.PI;
    }

    const isOnSite = isBuildingOnSite(object);
    (object.material as MeshBasicMaterial).color.set(isOnSite ? settings.normalColor : settings.warningColor);
        
    voxels.rotation.y = gridRotation;
    voxels.children.forEach(voxel => {
        const voxelCenter = new Vector3();
        voxel.getWorldPosition(voxelCenter);
        const inMesh = isInMesh(voxelCenter, object!, raycaster);
        voxel.visible = inMesh;
        if (inMesh) chooseVoxelsOnDevPad(voxel as Mesh, voxelCenter, gridRotation)
    });
}

function render() {
    renderer.render(scene, camera)
}

animate()