export enum RenderTypes {
    ClayModel = "Clay Model",
    ProgramColors = "Program Colors",
    Ghosted = "Ghosted",
  }
  
  export enum Tool {
    RenderType = "RenderType",
    EditProgram = "EditProgram",
    EditVoxel = "EditVoxel",
    EditFacade = "EditFacade",
    View = "View",
  }
  
  export enum ViewTool {
    Select = "Select",
    Pan = "Pan",
    Zoom = "Zoom",
    Orbit = "Orbit",
  }
  
  export enum Paint {
    Team = "Team",
    Meeting = "Meeting",
    Amenity = "Amenity",
    Core = "Core",
    Support = "Support",
    Remove = "Remove",
    None = "None",
    Events = "Events",
    CustomizedBusinessSupport = "CustomizedBusinessSupport",
  }
  
  export enum CursorTypes {
    default = "default",
    pointer = "pointer",
    orbit = "url(https://storage.googleapis.com/clay-production.appspot.com/designs/modify/orbit.svg), auto",
    zoomInOut = "url(https://storage.googleapis.com/clay-production.appspot.com/designs/modify/zoomInOut.svg), auto",
    zoomIn = "zoom-in",
    zoomOut = "zoom-out",
    grabbing = "grabbing",
    grab = "grab",
    notAllowed = "not-allowed",
    cell = "cell",
    wait = "wait",
  }
  
  export enum Colors {
    Amenity = 0xffbd02,
    AmenityHover = 0xffcf4d,
    Meeting = 0x2f81b8,
    MeetingHover = 0x5da5d5,
    Team = 0xa1d8e8,
    TeamHover = 0xc1e5f0,
    Core = 0x666666,
    CoreHover = 0x999999,
    Support = 0xbbbbbb,
    SupportHover = 0xdddddd,
    Events = 0x41c08b,
    EventsHover = 0x53fcb6,
    CustomizedBusinessSupport = 0xc86770,
    CustomizedBusinessSupportHover = 0xfa7f8b,
    Default = 0xaaaaaa,
    Line = 0xffffff,
    Site = 0xaae497,
    DevelopmentPad = 0x80ba6c,
    ClayWireframe = 0x888888,
    GhostWireframe = 0x666666,
    RemoveHover = 0xffb3b3,
    LineHover = 0x333333,
  }
  
  export enum Screen {
    home = "home",
    results = "results",
    modify = "modify",
    collection = "collection",
  }
  
  export const Modify = {
    program: "Edit Program",
    voxel: "Edit Voxel",
    facade: "Edit Facade",
  };
  
  export const ScreenTitles = {
    home: "Home",
    results: "Results",
    modify: "Modify",
    collection: "Collection",
  };
  