/**
 * @ol_path web/packages/clay/src/lib/designs/massing/utils.ts
 */

import { DesignsSite, Vec3 } from "../types";
import { cloneDeep, range, unzip } from "lodash";
import Flatten from "@flatten-js/core";

export function polygonToDesignsSite(poly: Flatten.Polygon): DesignsSite {
    const vertices: Vec3[] = poly.vertices.map(({ x, y }) => ({
      x,
      y,
      z: 0,
    }));
  
    return {
      ring: { edges: [], vertices: range(0, vertices.length) },
      vertices,
      edges: [],
      lat: 0,
      lng: 0,
    };
  }






