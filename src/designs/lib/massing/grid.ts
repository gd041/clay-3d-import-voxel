import { range, flatten as flattenOnce } from "lodash";
import Flatten from "@flatten-js/core";
import {
    utmProjectionStringFromLon,
    createProjection
} from '../geography/utils'

import { 
    Point,
    OffsetEdge,
    ProjectType
} from '../types'

import {
    model1,
    model2 
} from '../../../models/models'

import {
  calculateDefaultOffsetEdgesFromBrief,
  // OffsetEdge, --> i placed a simplified version in types
  performOffset,
} from '../../../briefs/lib/setbacks';

import { getBoundingBox } from '../geometry/geo'; // lib/geometry/geo

import { site1 } from '../../../models/briefs-test-data'

import { simplifyPolygon } from '../geometry/simplification' // ../designs/lib/simplification

import { metersToFeet, geoJsonToPoints } from '../geography/utils'



const { 
    geometry: { coordinates }
} = model2;

// const testPts: Point[] = coordinates[0][0] as any as Point[]
// const projection = createProjection(testPts[0])
// console.log(projection.forward)

export interface getBrief_brief_briefSite_zoning_setbacks_simplifiedPoly {
  __typename: "OffsetEdge";
  /**
   * The point at the start of this edge
   */
  point: number[];
  /**
   * The offset for this edge
   */
  offset: number;
}

export function anchorToOrigin(
  polygonPoints: Point[],
  gridDim: number
): [Flatten.Polygon, number, number[]] {
  const [backLeftPt, topRightPt] = getBoundingBox(polygonPoints);
  const halfspanX = (topRightPt[0] - backLeftPt[0]) / 2;
  const halfspanY = (topRightPt[1] - backLeftPt[1]) / 2;

  const anchorOffset = [halfspanX + backLeftPt[0], halfspanY + backLeftPt[1]];
  const offsetOp = (p: Point) =>
    [p[0] - anchorOffset[0], p[1] - anchorOffset[1]] as Point;

  const minRadius = Math.sqrt(halfspanX * halfspanX + halfspanY * halfspanY);
  const gridRadius = Math.ceil(minRadius / gridDim); // min number of grid cells to check along either axis

  const anchoredPolygonPairs = polygonPoints.map(offsetOp);
  const anchoredPolygonPts = anchoredPolygonPairs.map(
    (p) => new Flatten.Point(p[0], p[1])
  );
  return [new Flatten.Polygon(anchoredPolygonPts), gridRadius, anchorOffset];
}

export function projectAndOffset(
  inputEdges: OffsetEdge[]
): [Point[], proj4.Converter] {
  console.log('inputEdges \n', inputEdges)
  const points = inputEdges.map((e) => e.point as Point);
  const projection = createProjection(points[0]);
  const edges = points
    .map(projection.forward)
    //@ts-ignore
    .map(metersToFeet)
    .map((point, i) => ({
      point,
      offset: inputEdges[i].offset,
    }));
  // console.log('edges \n', edges)
  return [performOffset(edges), projection];
}
/**
 * 
 * @param brief 
 * @param gridDim 
 * @returns {GridPlanningOutputs} currently switched to any for proto
 */
export function generateGrid(
  brief: any, //Brief
  gridDim: number = 30
): any {
  const edges = calculateDefaultOffsetEdgesFromBrief(brief); // switched to just calculateDefaultOffset... for simplicity

  // Project edges to foot-space, perform polygon offset to get site envelope
  const [offsetPolygonPts, projection] = projectAndOffset(edges);

  // Anchor site envelope polygon to origin
  const [anchoredSetbackPolygon, gridRadius, anchorOffset] = anchorToOrigin(
    offsetPolygonPts,
    gridDim
  );

  // Project the original site to foot-space, and apply the same anchoring offset
  // switch brief.briefSite.site.geometry.siteBoundary ==> brief.geometry.siteBoundary
  const sitePoints = geoJsonToPoints(
    brief.geometry.siteBoundary
  );
  if (sitePoints.length > 2) sitePoints.pop();
  const sitePointsProjectedToFeet = sitePoints
    .map(projection.forward)
    .map(metersToFeet);
  const siteAnchored = sitePointsProjectedToFeet.map((p) => [
    p[0] - anchorOffset[0],
    p[1] - anchorOffset[1],
  ]);
  const anchoredSitePolygon = new Flatten.Polygon(
    siteAnchored.map((p) => new Flatten.Point(p[0], p[1]))
  );

  // currently commenting out potentially irrelevant functionality atm...

  // // Use the longest edge heuristic grid transform
  // const [gridTransform, gridCellPolygons, maxCellMap] =
  //   longestEdgeGridTransform(anchoredSetbackPolygon, gridDim, gridRadius);

  // // Trim the cell map
  // const [envelope, [leftColPadding, frontRowPadding]] = trimCellMap(maxCellMap);

  // const transformParams = {
  //   anchorLngLat: edges[0].point as [number, number],
  //   utmProjectionString: utmProjectionStringFromLon(edges[0].point[0]),
  //   anchorOffset: anchorOffset as [number, number],
  //   gridTransform,
  //   leftColPadding,
  //   frontRowPadding,
  // } as TransformParams;

  return {
    // envelope,
    // transformParams,
    anchoredSetbackPolygon,
    anchoredSitePolygon,
    // gridCellPolygons,
  };
}

// const { anchoredSetbackPolygon, anchoredSitePolygon } = generateGrid(site)
// console.log(JSON.stringify(anchoredSetbackPolygon, null, 4))
