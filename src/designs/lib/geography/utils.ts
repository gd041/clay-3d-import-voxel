//@ts-ignore
import proj4 from "proj4";

import { Point } from '../types'

import {
  EARTH_RADIUS_METERS,
  EARTH_RADIUS_PI,
  FEET_PER_METER,
} from '../geometry/constants';

const utmZoneFromLon = (lon: number): number => {
  return (Math.floor((lon + 180) / 6) % 60) + 1;
};

export const utmProjectionStringFromLon = (lon: number): string => {
  return `+proj=utm +zone=${utmZoneFromLon(
    lon
  )} +ellps=WGS84 +datum=WGS84 +units=m +no_defs`;
};

export const createProjection = (point: Point): proj4.Converter => {
  return proj4("WGS84", utmProjectionStringFromLon(point[0]));
};

export const metersToFeet = (point: Point): Point => [
  point[0] * FEET_PER_METER,
  point[1] * FEET_PER_METER,
];

export const feetToMeters = (point: Point): Point => [
  point[0] / FEET_PER_METER,
  point[1] / FEET_PER_METER,
];

export const feetPolyToWGS = (
  poly: Point[],
  project: proj4.Converter
): Point[] => {
  return poly.map(feetToMeters).map(project.inverse);
};

export const pointsToGeoJsonPolygon = (points: Point[]): string => {
  return JSON.stringify({
    type: "Feature",
    geometry: { type: "Polygon", coordinates: [points] },
    properties: null,
  });
};

export const pointsToGeoJsonLineString = (points: Point[]): string => {
  return JSON.stringify({
    type: "Feature",
    geometry: { type: "LineString", coordinates: points },
    properties: null,
  });
};

export const geoJsonToPoints = (geoJson: string): Point[] => {
  const parsed = JSON.parse(geoJson);
  const coords = parsed?.geometry?.coordinates[0];
  if (!coords) return [];

  return coords as Point[];
};

export const isNonEmptyGeojson = (g: string) => {
  const obj = JSON.parse(g);
  const coords = obj?.geometry?.coordinates[0];
  return coords.length && coords.length > 0;
};

// This function performs a simple projection from WGS84 to "feet-space"
// While not as accurate as proj4, it is sufficient for use cases like
// determining relative distances among several points, as in the
// useBuildingClosestTo massing hook.
export function simpleProjection([lon, lat]: Point): Point {
  const y =
    Math.log(Math.tan(((90 + lat) * Math.PI) / 360)) * EARTH_RADIUS_METERS;
  return [
    (EARTH_RADIUS_PI * lon * FEET_PER_METER) / 180,
    Math.max(-EARTH_RADIUS_PI, Math.min(y, EARTH_RADIUS_PI)) * FEET_PER_METER,
  ];
}

export function greatCircleDistance(
  lng1: number,
  lat1: number,
  lng2: number,
  lat2: number
): number {
  const p = 0.017453292519943295;
  const hav =
    0.5 -
    Math.cos((lat2 - lat1) * p) / 2 +
    (Math.cos(lat1 * p) *
      Math.cos(lat2 * p) *
      (1 - Math.cos((lng2 - lng1) * p))) /
      2;
  return 12742 * Math.asin(Math.sqrt(hav)); // returns distance in km
}
