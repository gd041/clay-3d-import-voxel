import * as THREE from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { DesignsSite, Vec3 } from "./types";
import { site1, site2 } from "../../models/briefs-test-data";
import { generateGrid } from "./massing/grid";
import { polygonToDesignsSite } from "./massing/utils";
import { createSiteMesh } from './scene-utils'
import { Colors } from '../lib/scene_const'

const { anchoredSetbackPolygon, anchoredSitePolygon } = generateGrid(site2)

console.log('devPadPoly \n', anchoredSetbackPolygon)
console.log('sitePoly \n', anchoredSitePolygon)

// function switchYZCoordinates()

const site = polygonToDesignsSite(anchoredSitePolygon)
export const devPad = polygonToDesignsSite(anchoredSetbackPolygon)

// console.log('site \n', site)
console.log('devPad \n', devPad)

export const siteMesh = createSiteMesh(site, Colors.Site, 0.1)
export const devPadMesh = createSiteMesh(devPad, Colors.DevelopmentPad, 0.5)




