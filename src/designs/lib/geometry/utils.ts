
/**
 * @ol_source web/packages/clay/src/lib/geometry/utils.ts
 * @description 
 * given a list returns a list of pairs where each pair is a continuous slice of the original list
 * including one pair that wraps around
 * @param list 
 * @returns 
 */
export default function windows<T>(list: T[]): [T, T][] {
    const result: [T, T][] = list.slice(1).map((o, i) => [list[i], o]);
    result.push([list[list.length - 1], list[0]]);
    return result;
  }
  