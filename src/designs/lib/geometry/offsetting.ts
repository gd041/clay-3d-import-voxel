import Flatten from "@flatten-js/core";
import { Point } from "../types";
import {
  AngleType,
  angleType,
  calcAngle,
  edgePairs,
  flattenEdgesToPoints,
  flattenPolyToEdges,
  getSegment,
  toPoly,
} from "./geo";

/**
 * @ol_source_for_all_functions_below web/packages/clay/src/lib/geometry/offsetting.ts
 */

// an axis aligned bounding box
export interface Bounds {
  xMin: number;
  yMin: number;
  xMax: number;
  yMax: number;
}

enum EdgeKind {
  SHADOW,
  OFFSET,
}

class EdgeData {
  public static shadowEdge(segment: Flatten.Segment): EdgeData {
    return new EdgeData(segment, EdgeKind.SHADOW, -1);
  }

  public static offsetEdge(segment: Flatten.Segment, offset: number): EdgeData {
    return new EdgeData(segment, EdgeKind.OFFSET, offset);
  }

  // eslint doesn't understand typescript's constructor property syntax so we're just turning that rule off here.
  // eslint-disable-next-line no-useless-constructor
  public constructor(
    public segment: Flatten.Segment,
    public kind: EdgeKind,
    public offset: number
  ) {
    // eslint-disable no-useless-constructor
    // eslint mistakenly believes this constructor is useless because it doesn't understand the typescript property
    // constructor syntax. We should fix this.
  }

  //@ts-ignore
  public get isShadow(): boolean {
    return this.kind === EdgeKind.SHADOW;
  }
}

class EdgeEntry {
  // eslint-disable-next-line no-empty-function, no-useless-constructor
  public constructor(private key: Flatten.Segment, private value: EdgeData) {}

  public get(key: Flatten.Segment): EdgeData | undefined {
    if (this.key.equalTo(key)) {
      return this.value;
    }

    return undefined;
  }

  public contains(key: Flatten.Segment): EdgeData | undefined {
    if (this.key.contains(key.start) && this.key.contains(key.end)) {
      return this.value;
    }

    return undefined;
  }
}

class OffsetData {
  // currently has an N lookup time so that we can do geometric equals with tolerance
  // could use a tree in the future?
  private storage: EdgeEntry[] = [];

  public loadExact(key: Flatten.Segment): EdgeData | undefined {
    const index = this.indexOf(key);
    return index === -1 ? undefined : this.storage[index].get(key);
  }

  public loadContains(key: Flatten.Segment): EdgeData | undefined {
    for (let i = 0; i < this.storage.length; i++) {
      const value = this.storage[i].contains(key);
      if (value != null) {
        return value;
      }
    }

    return undefined;
  }

  private indexOf(key: Flatten.Segment): number {
    for (let i = 0; i < this.storage.length; i++) {
      const entry = this.storage[i];
      if (entry.get(key) != null) {
        return i;
      }
    }

    return -1;
  }

  public store(data: EdgeData): this {
    const index = this.indexOf(data.segment);
    if (index === -1) {
      this.storage.push(new EdgeEntry(data.segment, data));
      return this;
    }

    this.storage[index] = new EdgeEntry(data.segment, data);
    return this;
  }
}

function segmentToLine(segment: Flatten.Segment): Flatten.Line {
  return new Flatten.Line(segment.tangentInStart().rotate90CW(), segment.start);
}

function handleCut(
  poly: Flatten.Polygon,
  s: Flatten.Point,
  e: Flatten.Point,
  offsets: OffsetData,
  box: Flatten.Box,
  grandParent?: Flatten.Polygon
): Flatten.Polygon {
  /* eslint-disable @typescript-eslint/no-use-before-define */
  // typescript-eslint/no-use-before-define doesn't allow for mutual recursion, which is required here
  // and a totally normal way to program.
  const [parent, child, ...others] = poly.cutFace(s, e);

  if (others.length) {
    throw new Error("cannot handle clips of more than one polygon yet");
  }

  if (!parent || !child) {
    throw new Error("clip failed");
  }

  // add shadow edge for the new segment added by the clip
  let edges = flattenPolyToEdges(parent);

  for (let i = 0; i < edges.length; i++) {
    const segment = getSegment(edges[i]);
    if (offsets.loadContains(segment) == null) {
      offsets.store(EdgeData.shadowEdge(segment));
    }
  }

  // validation that the shadow edge was handled
  edges = flattenPolyToEdges(parent);

  for (let i = 0; i < edges.length; i++) {
    if (offsets.loadContains(getSegment(edges[i])) == null) {
      throw new Error("failed to correctly add shadow edge");
    }
  }

  const newParent = analyzePoly(parent, offsets, box, grandParent);

  return analyzePoly(child, offsets, box, newParent);
}

function handleConcaveEdges(
  poly: Flatten.Polygon,
  l: Flatten.Edge,
  offsets: OffsetData,
  box: Flatten.Box,
  parent?: Flatten.Polygon
): Flatten.Polygon {
  const segment = getSegment(l);

  const line = new Flatten.Line(
    segment.tangentInStart().rotate90CW(),
    segment.start
  );

  const intersection = line
    .intersect(poly)
    .filter((p) => !segment.contains(p))
    .sort((a, b) => {
      const [d1] = a.distanceTo(segment.end);
      const [d2] = b.distanceTo(segment.end);
      return d1 - d2;
    })[0];

  return handleCut(
    poly.clone(),
    segment.end,
    intersection,
    offsets,
    box,
    parent
  );
}

function handleConvexPolygon(
  poly: Flatten.Polygon,
  offsets: OffsetData,
  box: Flatten.Box,
  parent?: Flatten.Polygon
): Flatten.Polygon {
  const nonShadowEdges = flattenPolyToEdges(poly).filter(
    (e) => !offsets.loadContains(getSegment(e))?.isShadow
  );

  const lines = nonShadowEdges.map((e) => segmentToLine(getSegment(e)));

  for (let i = 0; i < nonShadowEdges.length; i++) {
    const segment = getSegment(nonShadowEdges[i]);
    const data = offsets.loadContains(segment);
    if (!data || data.isShadow) {
      throw new Error("couldn't find edge data");
    }

    const normal = segment.tangentInStart().rotate90CW();
    lines[i] = segmentToLine(segment.translate(normal.multiply(-data.offset)));
  }

  const boxPoly = new Flatten.Polygon(box);

  const bspPolys = lines.map((line) => {
    const intersections = line.sortPoints(line.intersect(boxPoly));
    const [, interior] = boxPoly.cut(
      new Flatten.Multiline([line]).split(intersections)
    );

    return interior;
  });

  // TODO(dan): figure out if this is necessary
  if ((bspPolys as any).includes(undefined)) {
    return new Flatten.Polygon();
  }

  const parentEdges = parent
    ? flattenPolyToEdges(parent).filter(
        (e) => !offsets.loadContains(getSegment(e))?.isShadow
      )
    : flattenPolyToEdges(poly);

  const parentLines = parentEdges.map((e) => segmentToLine(getSegment(e)));

  const parentBspPolys = parentLines.map((line) => {
    const intersections = line.sortPoints(line.intersect(boxPoly));
    const [, interior] = boxPoly.cut(
      new Flatten.Multiline([line]).split(intersections)
    );

    return interior;
  });

  const contains = parentBspPolys.filter(
    (parentBspPoly) => parentBspPoly && parentBspPoly.contains(poly)
  );

  bspPolys.push(...contains);

  const interiorPoly = bspPolys.reduce((l, r) =>
    Flatten.BooleanOperations.intersect(l, r)
  );

  if (!interiorPoly || interiorPoly.area() === 0) {
    if (parent == null) {
      return new Flatten.Polygon();
    }
    return parent;
  }

  if (parent != null) {
    // TODO(Dan): There is probably a way to handle this better than bailing here
    if (Array.from(parent.faces).length === 0) {
      return new Flatten.Polygon();
    }

    if (
      Array.from(parent.faces)[0].orientation() !==
      Array.from(interiorPoly.faces)[0].orientation()
    ) {
      interiorPoly.reverse();
    }
    const unified = Flatten.BooleanOperations.unify(
      parent.clone(),
      interiorPoly.clone()
    );
    return unified;
  }

  return interiorPoly;
}

function analyzePoly(
  poly: Flatten.Polygon,
  offsets: OffsetData,
  box: Flatten.Box,
  parent?: Flatten.Polygon
): Flatten.Polygon {
  const pairs = edgePairs(poly);
  for (let i = 0; i < pairs.length; i++) {
    const [l, r] = pairs[i];
    if (angleType(calcAngle(l, r)) === AngleType.CONCAVE) {
      return handleConcaveEdges(poly, l, offsets, box, parent);
    }
  }

  return handleConvexPolygon(poly, offsets, box, parent);
}

export const ensureLastPointClosesPolygon = (points: Point[]): Point[] => {
  if (points.length < 3) {
    throw new Error("points not a polygon");
  }

  const firstPoint = points[0];
  const lastPoint = points[points.length - 1];
  if (firstPoint[0] === lastPoint[0] && firstPoint[1] === lastPoint[1]) {
    return points;
  }

  return [...points, firstPoint];
};

export default function offsetPolygon(
  points: Point[],
  offsets: number[],
  { xMin, yMin, xMax, yMax }: Bounds
): Point[] {
  if (points.length < 3 || offsets.length !== points.length) {
    throw new Error("invalid offsetting problem passed to offset polygon");
  }

  // convert the incoming points to a Flatten.Polygon
  const poly = toPoly(points);

  if (Array.from(poly.faces)[0].orientation() !== -1) {
    points.reverse();
    offsets.reverse();
    rotateRight(points);
    const out = offsetPolygon(points, offsets, { xMin, yMin, xMax, yMax });
    rotateLeft(points);
    rotateLeft(out);
    out.reverse();
    points.reverse();
    offsets.reverse();
    return out;
  }

  // store offset data for each edge in the polygon
  const offsetData = new OffsetData();

  const fedges = flattenPolyToEdges(poly);
  const fpoints = flattenEdgesToPoints(fedges);

  for (let i = 0; i < fpoints.length; i++) {
    // store offset data
    const edge = fedges[i];
    offsetData.store(EdgeData.offsetEdge(getSegment(edge), offsets[i]));
  }

  const box = new Flatten.Box(xMin, yMin, xMax, yMax);

  const inputPoly = toPoly(points);
  const outputPoly = analyzePoly(inputPoly, offsetData, box);

  return flattenEdgesToPoints(flattenPolyToEdges(outputPoly)).map(
    ({ x, y }) => [x, y]
  );
}

function rotateLeft<T>(a: T[]): T[] {
  const x = a.shift();
  if (x != null) {
    a.push(x);
  }
  return a;
}

function rotateRight<T>(a: T[]): T[] {
  const x = a.pop();
  if (x != null) {
    a.unshift(x);
  }
  return a;
}
