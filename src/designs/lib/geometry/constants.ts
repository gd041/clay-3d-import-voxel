/**
 * @source web/packages/clay/src/lib/constants.ts
 */

export const FEET_PER_METER = 3.28084;
export const METERS_PER_FOOT = 0.3048;

export const EARTH_RADIUS_METERS = 6378137.0;
export const EARTH_RADIUS_PI = EARTH_RADIUS_METERS * Math.PI;