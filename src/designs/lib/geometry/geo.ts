import Flatten from "@flatten-js/core";
import windows from "./utils";
import { Vec2, Vec3, Point} from '../types'

export const PRECISION = 1e5; // Epsilon tolerance for exact clipping tests (= 2x the clipping error)

export const EPSILON = 1 / PRECISION * 2; // Generous tolerance for matching up edges that may have shifted after heavy processing

/**
 * @ol_path_for_funcs_below_UNO web/node_modules/@outerlabs/isp/lib/geometry/geometry.js
 */

 export const isApprox = (x: any, y: any) => {
    // @ts-ignore
    var epsilon = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : EPSILON;
    return Math.abs(x - y) < epsilon;
  };

 var isSamePoint = function isSamePoint(a: any, b: any) {
    return isApprox(a[0], b[0]) && isApprox(a[1], b[1]) && isApprox(a[2] || 0, b[2] || 0);
  };


/**
 * @description get signed area of a closed path or polygon
 * !!question: if abs, sign is removed... look into this 
 * @param pathOrPolygon 
 * @returns 
 */
export const getArea = (pathOrPolygon: any): any => {
    return Math.abs(getSignedArea(pathOrPolygon))
}

/**
 * @description  get signed area of a closed path or polygon
 * @param pathOrPolygon 
 * @returns 
 */
export const getSignedArea = (pathOrPolygon: any) => {
// @ts-ignore
if (pathOrPolygon[0] != null && typeof pathOrPolygon[0][0] == 'number') {
    // @ts-ignore
    return getPathArea(pathOrPolygon);
} else {
    // Process polygon paths individually
    return pathOrPolygon.reduce(function (a: any, b: any) {
    return a + getPathArea(b);
    }, 0);
}
};

const forEdges = (path: any, f: any) => {
    var n = path.length - 1;
  
    for (var _i7 = 0; _i7 < n; ++_i7) {
      var run = f(path[_i7], path[_i7 + 1], _i7);
      if (run === false) break;
    }
  }; // Iterate over all distinct corners in a closed path.

export const getPathArea = (path: any) => {
    // Open paths have 0 area
    if (path.length && !isSamePoint(path[0], path[path.length - 1])) return 0;
    var area = 0; // Accumulate signed areas along the X axis.

    forEdges(path, function (a: any, b: any) {
        area += a[0] * b[1];
        area -= a[1] * b[0];
    });
    return area / 2;
}; // Get area of a shape (normalize if necessary)

export function getBoundingBox(points: Point[]): [Point, Point] {
  return points.reduce(
    ([[minX, minY], [maxX, maxY]], [x, y]) => [
      [Math.min(minX, x), Math.min(minY, y)],
      [Math.max(maxX, x), Math.max(maxY, y)],
    ],
    [
      [Number.POSITIVE_INFINITY, Number.POSITIVE_INFINITY],
      [Number.NEGATIVE_INFINITY, Number.NEGATIVE_INFINITY],
    ]
  );
}

/**
 * @ol_path_for_all_functions_below_UNO web/packages/clay/src/lib/geometry/geo.ts
 */

// Utility function for comparing two x-y coordinates
export const peq = (p1: Point, p2: Point) => p1[0] === p2[0] && p1[1] === p2[1];

// helper function to return a Flatten.Segment from a Flatten.Edge
export function getSegment(edge: Flatten.Edge): Flatten.Segment {
  if (!edge.isSegment) {
    throw new Error("unexpected arc");
  }
  return edge.shape;
}

// converts JS points to a Flatten.Polygon
export const toPoly = (points: Point[]): Flatten.Polygon =>
  new Flatten.Polygon(points);

// given a Flatten polygon with a single face, returns a list of edges
export const flattenPolyToEdges = (poly: Flatten.Polygon): Flatten.Edge[] =>
  Array.from(Array.from(poly.faces)[0] || []);

// given a list of edges, returns the start point of each edge
export const flattenEdgesToPoints = (edges: Flatten.Edge[]): Flatten.Point[] =>
  edges.map((e) => e.start);

// returns pairs of all adjacent edges in a polygon
export const edgePairs = (
  poly: Flatten.Polygon
): [Flatten.Edge, Flatten.Edge][] => windows(flattenPolyToEdges(poly));

// calculates the angle between two edges in degrees
export const calcAngle = (l: Flatten.Edge, r: Flatten.Edge): number =>
  getSegment(r).tangentInStart().angleTo(getSegment(l).tangentInEnd());

// calculates all of the interior angles of the polygon
export const calculateAngles = (poly: Flatten.Polygon): number[] =>
  edgePairs(poly).map(([l, r]) => calcAngle(l, r));

// the types of angles
export enum AngleType {
  CONVEX,
  COLINEAR,
  CONCAVE,
}

// returns if a given angle is concave or convex
export const angleType = (radians: number) => {
  if (Flatten.Utils.LT(radians, Math.PI)) {
    return AngleType.CONVEX;
  }
  if (Flatten.Utils.GT(radians, Math.PI)) {
    return AngleType.CONCAVE;
  }
  return AngleType.COLINEAR;
};

// Get axis-aligned bounding box for a given list of points.
export const getAABB = (points: Flatten.Point[]) => {
  const aabb = {
    xMin: Infinity,
    yMin: Infinity,
    xMax: -Infinity,
    yMax: -Infinity,
    xDim: 0,
    yDim: 0,
  };
  points.forEach((point) => {
    if (point.x < aabb.xMin) aabb.xMin = point.x;
    if (point.y < aabb.yMin) aabb.yMin = point.y;
    if (point.x > aabb.xMax) aabb.xMax = point.x;
    if (point.y > aabb.yMax) aabb.yMax = point.y;
  });
  aabb.xDim = aabb.xMax - aabb.xMin;
  aabb.yDim = aabb.yMax - aabb.yMin;
  return aabb;
};

// Given an array of vector representing vertices of an orthogonal polygon, create a transform
// function that rotates and translates those vertices so the polygon is aligned with the x-y
// axes, and its bounding box's bottom-left corner is at (0, 0). Also returns angle of rotation.
export const getTransform = (vecs: Vec3[]) => {
  let angle = Math.atan2(vecs[1].y - vecs[0].y, vecs[1].x - vecs[0].x);
  const rotatedPoints = vecs.map((vec) => {
    const p = Flatten.point(vec.x, vec.y);
    return p.rotate(-angle);
  });
  const { xDim, yDim } = getAABB(rotatedPoints);

  // Check if we are vertically aligned. If so, go horizontal i.e. rotate 90 degrees back toward zero.
  if (xDim < yDim) {
    if (angle > 0) angle -= Math.PI / 2;
    else angle += Math.PI / 2;
  }

  // Check if we are south aligned. If so, 180 no-scope.
  if (angle > Math.PI / 2) angle -= Math.PI;
  else if (angle < -Math.PI / 2) angle += Math.PI;

  // Apply the new rotation angle and get our origin point.
  const finalRotatedPoints = vecs.map((vec) => {
    const p = Flatten.point(vec.x, vec.y);
    return p.rotate(-angle);
  });
  const { xMin, yMin } = getAABB(finalRotatedPoints);

  return {
    angle: (-angle * 180) / Math.PI,
    transform: (vec: Vec3) => {
      const p = Flatten.point(vec.x, vec.y);
      const rp = p.rotate(-angle);
      const tp = rp.translate(-xMin, -yMin);
      return { x: Math.round(tp.x), y: Math.round(tp.y), z: vec.z };
    },
  };
};

export function distanceSquaredBetween(v1: Vec2, v2: Vec2): number {
  return (v2.x - v1.x) ** 2 + (v2.y - v1.y) ** 2;
}

export const vec3Distance = (v1: Vec3, v2: Vec3) =>
  Math.sqrt((v1.x - v2.x) ** 2 + (v1.y - v2.y) ** 2 + (v1.z - v2.z) ** 2);

// Calculates the weighted centroid a polygon.
// Copy-pasted from https://github.com/HarryStevens/geometric
// If we find ourselves using more from that repo, let's import it instead.
export function polygonCentroid(vertices: Point[]): Point {
  let a = 0;
  let x = 0;
  let y = 0;
  const l = vertices.length;

  for (let i = 0; i < l; i++) {
    const s = i === l - 1 ? 0 : i + 1;
    const v0 = vertices[i];
    const v1 = vertices[s];
    const f = v0[0] * v1[1] - v1[0] * v0[1];

    a += f;
    x += (v0[0] + v1[0]) * f;
    y += (v0[1] + v1[1]) * f;
  }

  const d = a * 3;

  return [x / d, y / d] as Point;
}
