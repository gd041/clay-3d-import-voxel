import { SimplifyAP } from "simplify-ts";
import { Point } from '../types';

export function simplifyPolygon(
  points: Point[],
  tolerance: number
): Point[] {
  return SimplifyAP(points, tolerance, true);
}
