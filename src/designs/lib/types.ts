
export interface Vec2 { 
    x: number,
    y: number
}

export interface Vec3 extends Vec2 { 
    z: number
}

export interface BaseEdge {
    from: number;
    to: number;
  }

export interface RingV2 {
edges: number[];
vertices: number[];
}

export interface DesignsSite {
ring: RingV2;
vertices: Vec3[];
edges: BaseEdge[];
lat: number;
lng: number;
}

export type Point = [number, number]


export type OffsetEdge = {
    point: number[];
    offset: number;
}

/**
 * @source web/packages/clay/src/queries/types/getSites.ts
 */

export interface getSites_sites {
__typename: "Site";
/**
 * The site's ID
 */
id: string;
/**
 * The display name of the site
 */
displayName: string;
/**
 * The formatted address for the site
 */
formattedAddress: string;
/**
 * The site type (GroundUp or AdaptiveReuse)
 */
siteType: ProjectType;
/**
 * The site geometry, incl lat/lng & boundary
 */
geometry: getSites_sites_geometry;
/**
 * Thumbnail URL
 */
thumbnail: string;
/**
 * The zoning constraints and targets associated with this site
 */
zoning: getSites_sites_zoning;
}

export interface getSites_sites_geometry {
__typename: "SiteGeometry";
/**
 * Latitude of the site anchor point
 */
lat: number;
/**
 * Longitude of the site anchor point
 */
lng: number;
/**
 * The site boundary, encoded as a JSON blob
 */
siteBoundary: string;
}

export interface getSites_sites_zoning_setbacks_simplifiedPoly {
__typename: "OffsetEdge";
/**
 * The point at the start of this edge
 */
point: number[];
/**
 * The offset for this edge
 */
offset: number;
}

export interface getSites_sites_zoning_setbacks {
__typename: "Setbacks";
/**
 * A polygon where each edge has an offset. This is meant to be a simplified version of the site boundary, along with
 * offset data
 */
simplifiedPoly: getSites_sites_zoning_setbacks_simplifiedPoly[];
}


export interface getSites_sites_zoning {
__typename: "Zoning";
/**
 * The Target FAR for the site (default from zoning code, user overridable)
 */
targetFAR: number;
/**
 * Maximum Height for the parcel (default from zoning code, user overridable)
 */
maxHeight: number;
/**
 * Site setbacks (default from zoning code, user overridable)
 */
setbacks: getSites_sites_zoning_setbacks;
/**
 * Maximum lot coverage (preset default, user overridable)
 */
maximumLotCoverage: number;
}

/**
 * @source web/packages/clay/src/types/graphql-global-types.ts
 */
export enum ProjectType {
    AdaptiveReuse = "AdaptiveReuse",
    GroundUp = "GroundUp",
  }