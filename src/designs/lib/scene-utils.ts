/**
 * @ol_path web/packages/clay/src/apps/designs/lib/scene-utils.ts
 */

import { Colors, CursorTypes, Paint, Tool, ViewTool } from './scene_const';
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import {
    MOUSE,
    Vector2,
    Vector3,
    Mesh,
    BufferGeometry,
    Color,
    MeshLambertMaterial,
    DoubleSide,
    BufferAttribute,
    LineBasicMaterial,
    Shape,
    ShapeGeometry,
    MeshBasicMaterial,
    BackSide,
    BoxGeometry,
    EdgesGeometry,
    LineSegments,
    Quaternion,
    Plane,
    Intersection,
    Object3D,
    
    } from "three";
  import { 
    Vec2, 
    Vec3,
    DesignsSite
    } from './types';

export const updateControls = (viewTool: ViewTool, controls: OrbitControls) => {
    switch (viewTool) {
      case ViewTool.Select:
        controls.mouseButtons.LEFT = -1;
        break;
      case ViewTool.Pan:
        controls.mouseButtons.LEFT = MOUSE.PAN;
        break;
      case ViewTool.Zoom:
        controls.mouseButtons.LEFT = MOUSE.DOLLY;
        break;
      case ViewTool.Orbit:
        controls.mouseButtons.LEFT = MOUSE.ROTATE;
        break;
      default:
        break;
    }
  };

  export const createSiteMesh = (
    site: DesignsSite,
    color: Colors,
    zOffset: number
  ): Mesh | undefined => {
    const shape = new Shape();
    const start = site.ring.vertices
      .map((v: number) => site.vertices[v])
      .reduce((s: null | Vec3, v: Vec3) => {
        if (s == null) {
          shape.moveTo(v.x, v.y);
          return v;
        }
        shape.lineTo(v.x, v.y);
        return s;
      }, null);
    if (start) {
      shape.lineTo(start.x, start.y);
      const geom = new ShapeGeometry(shape);
      const material = new MeshBasicMaterial({
        color,
        transparent: true,
        side: DoubleSide,
      });
      const mesh = new Mesh(geom, material).translateZ(zOffset);
      return mesh;
    }
    return undefined;
  };

