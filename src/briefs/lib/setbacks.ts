/**
 * @ol_path
 */

import { getArea } from "../../designs/lib/geometry/geo"; // import Point from types...

import { simplifyPolygon } from "../../designs/lib/geometry/simplification"
import offsetPolygon, {
  ensureLastPointClosesPolygon,
} from '../../designs/lib/geometry/offsetting'
import { getBoundingBox } from "../../designs/lib/geometry/geo";
import {
  feetPolyToWGS,
  metersToFeet,
  createProjection,
} from "../../designs/lib/geography/utils";

import { Point, OffsetEdge } from '../../designs/lib/types';

import { site1 } from '../../models/briefs-test-data'


// types that'll probably be commented out and replaced with custom, simplification for MVP:
// import { OverrideState } from "types/graphql-global-types";
// import { getBrief_brief as Brief } from "queries/types/getBrief";
// import { allOfBrief_briefSite_zoning_setbacks_simplifiedPoly as gqlOffsetEdge } from "queries/types/allOfBrief";

// export type OffsetEdge = Omit<gqlOffsetEdge, "__typename">;

/**
 * @param {Brief} brief object output from briefs that contains
 * site, zoning, etc to create site boundary & development pad.
 */
export function calculateDefaultOffsetEdgesFromBrief(
  brief: any // Brief
): OffsetEdge[] {
  const { setbacks } = brief.zoning; // brief.briefSite.zoning
  const points = setbacks.simplifiedPoly.map((sp: any) => sp.point as Point);
  const project = createProjection(points[0]);
  const utmPoints = points.map(project.forward);
  const simplifiedBoundaryInMeters = simplifyPolygon(utmPoints, 5);
  const simplifiedSiteBoundary = simplifiedBoundaryInMeters.map(
    project.inverse
  );

  //@ts-ignore
  return simplifiedSiteBoundary.map((point) => ({
    point,
    offset: Math.round(setbacks.simplifiedPoly[0].offset),
  }));
}

// const edges: any = calculateDefaultOffsetEdgesFromBrief(site)
// console.log('calculateDefaultOffsetEdgesFromBrief', edges)

export function calculateAreaFromOffsetEdges(
  offsetEdges: OffsetEdge[]
): number {
  const points = offsetEdges.map((s) => s.point as Point);
  const projection = createProjection(points[0]);
  //@ts-ignore
  const pointsInFeet = points.map(projection.forward).map(metersToFeet);
  const offsets = offsetEdges.map((s) => s.offset);
  const [[xMin, yMin], [xMax, yMax]] = getBoundingBox(pointsInFeet);
  let poly;
  try {
    poly = offsetPolygon(pointsInFeet, offsets, {
      xMin,
      xMax,
      yMin,
      yMax,
    });
    return getArea([...poly, poly[0]]);
  } catch (e) {
    console.error(e);
    return 0;
  }
}

// /**
//  * 
//  * @param {Brief} brief
//  * @returns 
//  */
// export function calculateOffsetEdgesFromBrief(brief: any): OffsetEdge[] {
//   const { setbacks } = brief.briefSite.zoning;
//   return setbacks.overridden === OverrideState.Default
//     ? calculateDefaultOffsetEdgesFromBrief(brief)
//     : setbacks.simplifiedPoly.map((sp) => ({
//         point: sp.point as Point,
//         offset: sp.offset,
//       }));
// }

export function performOffset(edges: OffsetEdge[]): Point[] {
  const offsets = edges.map((e) => e.offset);
  const points = edges.map((e) => e.point as Point);

  const [[xMin, yMin], [xMax, yMax]] = getBoundingBox(points);
  let poly;
  try {
    poly = offsetPolygon(points, offsets, {
      xMin,
      xMax,
      yMin,
      yMax,
    });
  } catch (e) {
    console.error(e);
    return [];
  }

  if (poly.length > 0) {
    return ensureLastPointClosesPolygon(poly);
  }

  return [];
}

export function performOffsetOnLatLngOffsetEdges(
  inputEdges: OffsetEdge[]
): Point[] {
  const points = inputEdges.map((e) => e.point as Point);
  const projection = createProjection(points[0]);
  const edges = points
    .map(projection.forward)
    //@ts-ignore
    .map(metersToFeet)
    .map((point, i) => ({
      point,
      offset: inputEdges[i].offset,
    }));
  return feetPolyToWGS(performOffset(edges), projection);
}
