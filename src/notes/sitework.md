
# files to create site geometry in three :
- setbacks.ts, geo.ts, offsetting, etc...
- - informs **grid.ts**
- **grid.ts** 
- -  contains **generateGrid()**
- - - -  **generateGrid()** --> returns [ **anchoredSetbackPolygon**, **anchoredSitePolygon** ]
- [web/packages/clay/src/lib/designs/massing/utils.ts] then applied in [web/packages/clay/src/lib/designs/massing/massing-generator.ts]
- - **polygonToDesignsSite()** creates designSuite Object for **site** & **developmentPad**
- [web/packages/clay/src/apps/designs/lib/scene-utils.ts]
- - **createSiteMesh()** consumes *DesignsSuite* to create **mesh**
- web/packages/clay/src/apps/designs/lib/scene.ts
- - *n/a at the moment*
---
# files to render geometry in react:

- web/packages/clay/src/apps/admin/generate-massings/site-viewer.tsx
- - 
- web/packages/clay/src/apps/admin/generate-massings/building-viewer.tsx
- - 
- web/packages/clay/src/apps/admin/generate-massings/generate-massings.tsx
- - 
---
# app.tsx --> entry point

- web/packages/clay/src/apps/admin/app.tsx
